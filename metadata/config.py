import traceback
from importlib import import_module
from django.contrib.contenttypes.models import ContentType
from django.db import connection
from .models import MetaMenu, MetaAction, MetaView


def auto_discover_metadata(run=False):
    """
    Auto-discover INSTALLED_APPS modules and fail silently when
    not present. This only executes when run = True
    """
    if run:
        from django.apps import apps
        v_ids = []  # view ids
        a_ids = []  # action ids
        m_ids = []  # menu ids
        # Attempt to import the app's admin module
        for app_config in apps.get_app_configs():
            # pylint: disable=broad-except
            try:
                if 'django' in app_config.name:
                    continue  # ignore django apps
                if app_config.name in ['rest_framework', 'corsheaders', 'comun']:
                    continue  # ignore not modeling apps
                print('\033[94m' + '\033[1m' +
                      '\n-------------------' + '\033[0m')
                print(app_config.name + ':')
                v_ids.extend(discover_views(app_config))
                a_ids.extend(discover_actions(app_config))
                m_ids.extend(discover_menus(app_config))

            except ModuleNotFoundError as module_not_found:
                print('ModuleNotFoundError: ' + str(module_not_found))
                pass

            except Exception:
                print('\033[93m' + traceback.format_exc() + '\033[0m')
                # we just ignore the error, because we dont care about
                # the modules have the admin file, and what they does in there,
                # we only are interested in getting the metada
                pass

        # DELETE RECORDS THAT NOT EXIST IN .py FILES (like menu.py)
        MetaView.objects.exclude(id__in=v_ids).delete()
        MetaAction.objects.exclude(id__in=a_ids).delete()
        MetaMenu.objects.exclude(id__in=m_ids).delete()
        set_parent_paths()  # must be done after all menus have been saved
        print('\033[94m' + '\033[1m' + '-------------------' + '\033[0m')


def discover_views(app_config):
    """
    discover views in /meta/view.py in each app and update database.
    the file view.py must have an VIEW array of dicts (each dict is a view)
    """

    module = import_module('%s.meta.view' % app_config.name)
    views = getattr(module, 'VIEW')
    ids = []

    for view in views:
        print(view['ref'])
        app_model = view['model_ref'].split('.')

        # pylint: disable = unused-variable
        obj, created = MetaView.objects.update_or_create(
            # outside defaults must be unique
            ref=view['ref'],
            defaults={
                'name': view['name'],
                'model_id': ContentType.objects.get_by_natural_key(app_model[0], app_model[1]),
                'view_type': view['view_type'],
                'arch': view['arch']
            }
        )
        ids.append(obj.id)

    return ids


def discover_actions(app_config):
    """
    discover actions in /meta/action.py in each app and update database.
    the file action.py must have an ACTION array of dicts (each dict is an action)
    """

    module = import_module('%s.meta.action' % app_config.name)
    actions = getattr(module, 'ACTION')
    ids = []

    for action in actions:
        print(action['ref'])
        views = MetaView.objects.filter(ref__in=action['view_refs'])

        # pylint: disable = unused-variable
        obj, created = MetaAction.objects.update_or_create(
            # outside defaults must be unique
            ref=action['ref'],
            defaults={
                'name': action['name'],
                'context': action.get('context', None),
                'view_mode':  action['view_mode'],
                'arch': join_archs(views, action.get('context', None))
            }
        )
        obj.view_ids.set(views)
        ids.append(obj.id)

    return ids


def discover_menus(app_config):
    """
    discover menus in /meta/menu.py in each app and update database.
    the file menu.py must have a MENU array of dicts (each dict is a menu)
    """

    module = import_module('%s.meta.menu' % app_config.name)
    menus = getattr(module, 'MENU')
    ids = []

    # CREATE OR UPDATE RECORDS
    for menu in menus:
        print(menu['ref'])

        # pylint: disable = unused-variable
        obj, created = MetaMenu.objects.update_or_create(
            # outside defaults must be unique
            ref=menu['ref'],
            defaults={
                'name': menu['name'],
                'menu_type': menu['menu_type'],
                'sequence': menu['sequence'],
                'icon': menu.get('icon', None),
                'client_route': menu.get('client_route', None),
                'action_id': get_ref(menu, 'action_ref', MetaAction),
                'parent_id': get_ref(menu, 'parent_ref', MetaMenu),
                'initial_route': None,  # reset initial_routes
                'parent_path': None,  # reset paths
                'depth': None  # reset depths
            }
        )
        ids.append(obj.id)

    # SET INITIAL ROUTES
    for menu in menus:
        if menu.get('menu_type', None) == 'module':
            initial = get_ref(menu, 'initial_ref', MetaMenu)
            if initial:
                MetaMenu.objects.filter(ref=menu['ref']).update(
                    initial_route=initial.menu_type + '/' + initial.client_route
                )

    return ids


# useful methods

def set_parent_paths():
    """ compute paths en each menu record and save them to db """
    with connection.cursor() as cursor:
        cursor.execute("""
            UPDATE metadata_metamenu
            SET parent_path = res.path, depth = res.depth
            FROM (
                    WITH RECURSIVE nodes(id, depth, path) AS (
                            SELECT mm.id, 1::INT AS depth, mm.id::TEXT AS path
                            FROM metadata_metamenu AS mm
                            WHERE mm.parent_id_id IS NULL
                        UNION ALL
                            SELECT c.id, p.depth + 1 AS depth, (p.path || '/' || c.id::TEXT)
                            FROM nodes AS p, metadata_metamenu AS c
                            WHERE c.parent_id_id = p.id
                    ) SELECT * FROM nodes
            ) AS res
            WHERE
                res.id = metadata_metamenu.id""")


# pylint: disable=unused-argument
def join_archs(views, context=None):
    """
    Allow to join arch of the views (for efficient process) and apply context
    TODO: apply context
    """
    arch = {}
    for view in views:
        model_ref = view.model_id.app_label + '.' + view.model_id.model
        arch[model_ref] = {
            **arch.get(model_ref, {}),
            view.view_type: view.arch
        }
    return arch


def get_ref(base_dict, key, model):
    """ get record based on ref """
    # pylint: disable=cell-var-from-loop
    reference = base_dict.get(key, None)
    if reference:
        try:
            reference = model.objects.get(ref=reference)
        except model.DoesNotExist:
            print(
                f'\033[93mERROR in [{base_dict["ref"]}]: reference[{reference}] doesn\'t exist in DB. Change the order.\033[0m')
            reference = None
    return reference
