from rest_framework import serializers
from .models import MetaMenu, MetaAction


class MetaActionSerializer(serializers.ModelSerializer):
    """ Serializer to have a MetaAction list"""

    class Meta:
        model = MetaAction
        fields = [
            'id',
            'ref',
            'name',
            'view_mode',
            'arch'
        ]


class MetaMenuSerializer(serializers.ModelSerializer):
    """ Standard Serializer for menus """

    class Meta:
        model = MetaMenu
        fields = [
            'id',
            'ref',
            'name',
            'menu_type',
            'sequence',
            'icon',
            'client_route',
            'initial_route',
            'action_id',
            'parent_id',
            'parent_path',
            'depth',
        ]
