from django.apps import AppConfig
from os import environ


class MetadataConfig(AppConfig):
    name = 'metadata'

    def ready(self):
        # JOBS: remember to put scheduler app, first at INSTALLED_APPS because
        # instance_all_jobs needs job_setup that resides in scheduler app, and
        # because run_once_process var is needed, to instance jobs only once.
        if environ.get('RUN_MAIN', None) != 'true':
            # from . import jobs
            # jobs.instance_jobs()

            # load metadata from apps
            from . import config as meta_config
            meta_config.auto_discover_metadata(True)
