from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.db import models


def dfield(fieldname, label='', placeholder='', required=True, readonly=False, visible=True, options=None):
    """ Allow to generate dict of fields easily """
    return {
        'fieldname': fieldname,
        'label': label,
        'placeholder': placeholder,
        'required': required,
        'readonly': readonly,
        'visible': visible,
        'options': options,  # for any kind of field
        'widget': None,
    }


def m2o(fieldname, label='', placeholder='', required=True, readonly=False, visible=True, options=None, api_route=''):
    """ Allow to generate dict of relational field easily"""
    return {
        'fieldname': fieldname,
        'relation': 'many2one',
        'label': label,
        'placeholder': placeholder,
        'required': required,
        'readonly': readonly,
        'visible': visible,
        'options': options,  # for any kind of field
        'domain': None,  # for related fields
        'api_route': api_route,
    }


def o2m(fieldname, label='', editable=True, fields=None, api_route=''):
    """ Allow to generate dict of relational field with subfields easily"""
    return {
        'fieldname': fieldname,
        'relation': 'one2many',
        'label': label,
        'editable': editable,
        'fields': fields,
        'api_route': api_route,
    }

def m2m(fieldname, label='', editable=True, fields=None, api_route=''):
    """ Allow to generate dict of relational field with subfields easily"""
    return {
        'fieldname': fieldname,
        'relation': 'one2many',
        'label': label,
        'editable': editable,
        'fields': fields,
        'api_route': api_route,
    }


class MetaView(models.Model):
    """
    Representa los fields que se incluiran en las vistas, asi como los parametros
    necesarios para que se pueda presentar la informacion. Cada vista tiene un arch
    y este se forma por un algoritmo, cada arch necesita data segun cada vista:

    list
        arguments (apiRoute, domain, limit)
        search (campos para filtrar, filtros y grupos)
        fields (tuplas)

    form
        arguments (apiRoute, record_id)
        header (buttons, status_bar)
        fields (tuplas)
    """

    ref = models.CharField(
        'Referencia',
        unique=True, null=True, max_length=100,
        db_index=True, editable=False)  # like id
    name = models.CharField('Nombre', max_length=100)  # a simple description
    model_id = models.ForeignKey(
        ContentType,
        models.PROTECT,
        null=True)
    view_type = models.CharField(
        'Tipo de vista',
        max_length=14,
        choices=[
            ('list', 'Lista'),
            ('form', 'Formulario'),
            ('kanban', 'Kanban'),
            ('calendar', 'Calendario')
        ])
    arch = JSONField('Arquitectura', null=True)


class MetaAction(models.Model):
    """
    Each action describe how a model or models are presented in frontend through views.
    Una action puede ser un conjunto de vistas, o una sola vist. El campo ARCH se calcula
    en base a los otros arch de view_ids para hacer mas eficiente el proceso.

    action
        ref
        name
        context (optional)
        view_mode
        view_refs
    """

    ref = models.CharField(
        'Referencia',
        unique=True, null=True, max_length=100,
        db_index=True, editable=False)  # like id
    name = models.CharField('Nombre', max_length=100)  # a simple description
    context = JSONField('Contexto', null=True)  # have data that affect views

    # model order of views
    # the format is "app.model1[view_type1,view_type2..];app.model2..."
    # sirve para multiple models para escpeficar cada model el orden de sus vistas
    # si no hay un orden (detro de los []) significa que no se necesita
    view_mode = models.CharField('Modo', max_length=100)
    view_ids = models.ManyToManyField(MetaView)
    arch = JSONField('Arquitectura', null=True)  # view_ids archs joined


class MetaMenu(models.Model):
    """
    Sirve para indicar el "routing" en el cliente (menu_type=module)
    y/o para ejecutar una accion (menu_type=in-module), el otro (m_type=desk)
    sirve solamente para agrupar botones en el desk.

    OJO: en in-module se utiliza un tipo que este acorde al action.
    Los campos que se ocupan son:

    type = desk
        ref
        name
        menu_type = desk
        sequence

    type = module
        ref
        name
        menu_type = module
        sequence
        icon
        client_route
        initial_ref (a menu ref)
        parent_ref (a menu ref)

    type = model/dashboard/etc (in-module)
        ref
        name
        menu_type = model/dashboard/etc
        sequence
        icon
        client_route
        action_ref (an action ref)
        parent_ref (a menu ref)
    """

    # menu_

    ref = models.CharField(
        'Referencia',
        unique=True, null=True, max_length=100,
        db_index=True, editable=False)  # like id
    name = models.CharField(
        'Nombre',
        max_length=100)  # displayName
    menu_type = models.CharField(
        'Tipo',
        max_length=14,
        choices=[
            ('desk', 'Desk'),  # para agrupar
            ('module', 'Module'),  # botones del desk
            ('model', 'Model'),  # in-module
            ('dashboard', 'Dashboard')  # in-module
        ])

    sequence = models.PositiveSmallIntegerField(default=0)
    icon = models.CharField('Icono', max_length=50, null=True)

    client_route = models.CharField(
        'Client Route',
        max_length=50, null=True)
    initial_route = models.CharField(  # initial_ref = initial redirect, like user click a menu
        'Client Initial Module Route',
        max_length=50, null=True)
    action_id = models.ForeignKey(  # action_ref
        MetaAction,
        on_delete=models.CASCADE,
        null=True)
    parent_id = models.ForeignKey(  # parent_ref
        'self',
        on_delete=models.CASCADE,
        null=True)  # recursive

    # allow recursive efficient access
    parent_path = models.CharField('Parent Path', max_length=50, null=True)
    depth = models.PositiveSmallIntegerField(null=True)
