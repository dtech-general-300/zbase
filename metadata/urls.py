from django.urls import include, path
from rest_framework import routers
from . import views

ROUTER = routers.SimpleRouter()
ROUTER.register(r'metaaction', views.MetaActionViewSet, basename="metaaction")
ROUTER.register(r'metamenu', views.MetaMenuViewSet, basename="metamenu")


# Namespace
# don't use namespaces with HyperlinkedSerializers
# HyperlinkedSerializer: is the url with the id
app_name = 'metadata'

# Urls
urlpatterns = [
    path('', include(ROUTER.urls)),
]
