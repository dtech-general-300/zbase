# from rest_framework.response import Response
# from rest_framework.decorators import action
# from rest_framework import status
from comun.views import DViewSet
from .serializers import MetaActionSerializer, MetaMenuSerializer
from .models import MetaMenu, MetaAction


class MetaActionViewSet(DViewSet):
    """
    API endpoint: return a filtered list of metaActions.
    """
    query_model = MetaAction
    serializer_class = MetaActionSerializer
    pagination_class = None


# TODO: maybe is better only one request to action that return the initial data too

#     # pylint: disable=unused-argument, invalid-name
#     @action(detail=True)
#     def get_action(self, request, pk=None):
#         """ get single action + initial records TODO: error handler """
#         instance = self.get_object()
#         serializer = self.get_serializer(instance)
#         return Response({
#             'action': serializer.data,
#             'data': initial_data(instance)
#         })


# def initial_data(instance):
#     """ allow to retrieve initial data for an action, to avoid sequencial intial request """
#     data = {}
#     for model, views in instance.arch.items():
#         for name, view in views.items():
#             print(name)
#         # model_ref = view.model_id.app_label + '.' + view.model_id.model
#         # arch[model_ref] = {
#         #     **arch.get(model_ref, {}),
#         #     view.view_type: view.arch
#         # }
#     return data
#     # print(instance.view_mode)
#     # print(instance.context)


class MetaMenuViewSet(DViewSet):
    """
    API endpoint: handle all menus of the system
    """
    query_model = MetaMenu
    serializer_class = MetaMenuSerializer
    pagination_class = None
