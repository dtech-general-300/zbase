import time
import logging
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.postgres.fields import JSONField
from . import config

LOGI = logging.getLogger("info")  # debug info warning
LOGX = logging.getLogger("error")  # error critical


# MODELS

class Jobs(models.Model):
    """
    this model, stores the jobs with an 'id' (ref), function location (func_ref),
    and a trigger with arguments, in order to instantiate a job at app start.
    """

    # A simple name of the job
    name = models.CharField('Nombre', max_length=100)

    # represent the unique id (of the job) inside the scheduler # unicode string
    ref = models.CharField('Referencia', max_length=50, unique=True)

    # textual ref of the function (where is the function) in the format 'app.file:function_name'
    func_ref = models.CharField(
        'Referencia textual de la función', max_length=70)

    # defines when the job is called, it could be: interval, cron, date
    trigger = models.CharField('Trigger', max_length=20)

    # next scheduled run time of the job # null=blank=True to permit any 'no data' posibility
    next_time = models.DateTimeField(
        'Siguiente Ejecución', null=True, blank=True)

    # the current state of the job
    state = models.CharField('Estado', max_length=10, default='stopped', choices=[
        ('running', 'Ejecutándose'),
        ('stopped', 'Detenido')
    ])

    # argumentos para el trigger, estos especifican los tiempos del trigger
    # TODO DjangoJSONEncoder for datetimes
    trigger_kwargs = JSONField('Período del trigger', null=True)

    # constructor (override models.Model constructor)
    def __init__(self, *args, **kwargs):
        # declare an 'private instance attribute' (attribute from an instance of the class Jobs).
        # This attribute is accesible from object not from class
        self.__job = None
        # need super to avoid crushes in django model.Models
        super(Jobs, self).__init__(*args, **kwargs)

    # representation (nombre del objeto en python), to get call str(object_instance)
    def __str__(self):
        return self.name

    @property
    def job(self):
        """
        @property: access the attributte (get)
        when call self.job in any function, this @property (getter)
        first set the attribute with job object from scheduler
        """
        if not self.__job:
            # return the job based on 'id' (ref)
            self.__job = config.ISCHEDULER.get_job(self.ref)
        return self.__job

    @job.setter
    def job(self, job_instance):
        # set the attribute (set)
        # job_instance of 'scheduler job', not instance of 'class Jobs'
        self.__job = job_instance

    def start(self):
        """ the start method, means the job is added in scheduler """
        self.job = config.ISCHEDULER.add_job(
            self.func_ref,
            self.trigger,
            id=self.ref,
            # replace an existing instance of job with the same 'id' (ref)
            replace_existing=True,
            **self.trigger_kwargs)  # keyworded variable-lenght argument list.

        self.state = 'running'
        # TODO user can edit when is the next run time of job
        self.next_time = self.job.next_run_time
        self.save()  # save in the database

    def stop(self):
        """ the stop method, means the job is removed in scheduler """
        try:
            self.job.remove()  # remove job from scheduler
            self.state = 'stopped'
            self.next_time = None
            self.save()  # save in the database
        except:
            LOGX.error('Error: job has not been stopped. Id: %s', self.ref)


# METHODS

def job_setup(func_ref, name, trigger, args, state='stopped'):
    """
    this method add a new or get an existing job in the database
    'get_or_create' avoid th race condition in multiple requests
    get uses all fields written except defaults, create uses all
    fields including defaults.\n\n

    func_ref: represent the unique id (of the job) inside the scheduler\n
    name: a simple name for the job\n
    trigger: 'interval', 'cron', 'date'\n
    args: trigger dict args\n
    state: running | default: stopped\n
    """

    # the function name will be the 'id' (ref) of the job in the scheduler
    func_name = func_ref.split(':')[1]

    # pylint: disable=unused-variable
    job_record, created = Jobs.objects.get_or_create(
        ref=func_name,  # this must be a unique field, to avoid repeated record
        defaults={  # defaults just works when created
            'name': name,
            'func_ref': func_ref,
            'trigger': trigger,
            'next_time': None,
            'state': state,
            'trigger_kwargs': args,
        }
    )

    # only add the job (start) to scheduler when it was running
    if job_record.state in 'running':
        job_record.start()


# DECORATORS

def scheduler_job(func):
    """
    this decorator runs, when the job is called from scheduler
    this decorator add logs with time of function, and updates next_time   
    """
    def wrapper(*args, **kwargs):
        # get record
        try:
            # el nombre de la funcion es el 'id' (ref)
            job_record = Jobs.objects.get(ref=func.__name__)
        except ObjectDoesNotExist:
            LOGX.error(
                f'JOB ERROR | Cannot get job record from DB. Id: {func.__name__}')
            return None

        # log: start
        LOGI.info(f'JOB START | {func.__name__}')

        # run function and measure time
        begin = time.time()
        res = func(*args, **kwargs)
        end = time.time()

        # save next_run_time (synchro scheduler job instance with record in the database)
        job_record.next_time = job_record.job.next_run_time
        job_record.save()

        # log: job done
        LOGI.info(
            f'JOB DONE  | {func.__name__} - Time: {end - begin:.4f} sec.')
        return res
    return wrapper
