import logging
from pytz import utc
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor

LOGI = logging.getLogger("info")  # debug info warning
LOGX = logging.getLogger("error")  # error critical


# global variables
ISCHEDULER = None  # instance of the Background scheduler
# indicates if the code is running in the process which run only once.
RUN_ONCE_PROCESS = False


def init_background():
    """ Create an instance of Background Scheduler and start it """
    LOGI.info(':: Background Scheduler Start ::')

    # global allows to work with the outside variable
    global ISCHEDULER

    # instance of BackScheduler (the background sch. is the
    # assembly of triggers, executors, jobstores, etc.)
    ISCHEDULER = BackgroundScheduler(
        jobstores={
            # MemoryJobStore: when application start, recreates the jobs.
            # Stores jobs in an array in RAM. Doesn't has persistence support.
            # Stores jobs in memory as-is, without serializing them.
            'default': MemoryJobStore(),
        },
        executors={
            # Executors: Handle the running of the jobs. When job is done, the
            # executor notify the Scheduler which then emits an event. U can
            # choose how many threads and how many process(cores) u want to use.
            'default': ThreadPoolExecutor(10),
            'processpool': ProcessPoolExecutor(1)
        },
        # default values for newly added jobs
        job_defaults={
            # Coalesce: join (fusion) overlaped jobs
            # If coalesce = True, then when the scheduler sees one or more queued
            # executions for a particular job, it will only trigger it once.
            'coalesce': True,
            # Max Instances: How many instances of each job are allowed to be run
            # at the same time. This will be the default value of max number of
            # concurrently job's instances.
            'max_instances': 1,
            # Misfire grace time: lapso de tiempo (posterior al runtime del job)
            # que acepta el scheduler para volver a intentar correr un job.
            # 'misfire_grace_time': seconds
        },
        timezone=utc  # time reference to run the jobs, utc means utc+0
    )

    # start the scheduler, after this, the scheduler config can not be altered.
    ISCHEDULER.start()
