from django.apps import AppConfig
from os import environ


class SchedulerConfig(AppConfig):
    name = 'scheduler'

    # ready method: it runs when django starts, use this method to perform
    # initialization task like registering signals or toher things.
    # Django has an autoreload mechanism, when django starts, two processes
    # start, one is responsible for monitoring file changes, and the other
    # is the main process. If the file changes, it will exit the current
    # main process and restart another child main process.
    def ready(self):
        # the code here, will run twice at django start and once in every autoreload
        # with --noreload: just tun once at django start

        if environ.get('RUN_MAIN', None) != 'true':
            # the code here, will run once at django start
            # with --noreload: just tun once at django start
            from . import config
            config.RUN_ONCE_PROCESS = True  # is true in a process that run only one time
            config.init_background()  # here we start the scheduler only once

        if environ.get('RUN_MAIN', None) == 'true':
            # the code here, will run once at django start and in every autoreload
            # with --noreload: will never run
            pass
