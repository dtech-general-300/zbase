# Generated by Django 3.0 on 2020-01-18 07:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0002_productattributeline'),
    ]

    operations = [
        migrations.AddField(
            model_name='productattributeline',
            name='name',
            field=models.CharField(max_length=100, null=True, verbose_name='Nombre Línea'),
        ),
    ]
