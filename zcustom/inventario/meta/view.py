from metadata.models import dfield, m2o, o2m, m2m

VIEW = [
    # PRODUCTOS

    # producto
    dict(
        ref='list_view_standard_inventario_product',
        name='lista estandar para inventario.product',
        model_ref='inventario.product',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/products/',
                'new': 'Crear un nuevo Producto'
            },
            'search': {
                'fields': ['name'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                dfield('name', 'Nombre')
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_product',
        name='form estandar para inventario.product',
        model_ref='inventario.product',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/products/',
                'form_path': 'inventario.product'
            },
            'header': [
            ],
            'fields': [
                dfield('name', 'Nombre del Producto'),
                o2m('attribute_line_ids', fields=[
                    dfield('name', 'Nombre de línea'),
                ]),
            ]
        }
    ),

    # product attribute lines
    dict(
        ref='list_view_standard_inventario_productattributeline',
        name='lista estandar para inventario.productattributeline',
        model_ref='inventario.productattributeline',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/atributos/',
                'new': 'Crear un nuevo Atributo'
            },
            'search': {
                'fields': ['name'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                # dfield('str_name', 'Nombre')
                dfield('name', 'Nombre')
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_productattributeline',
        name='form estandar para inventario.productattributeline',
        model_ref='inventario.productattributeline',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/atributos/',
                'form_path': 'inventario.productattributeline'
            },
            'header': [
            ],
            'fields': [
                dfield('name', 'Nombre del Atributo'),
                m2o('product_id', 'Producto', api_route='/inventario/products/')
            ]
        }
    ),



    # PATRONES


    # patron atributo
    dict(
        ref='list_view_standard_inventario_patronatributo',
        name='lista estandar para inventario.patronatributo',
        model_ref='inventario.patronatributo',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/patronatributo/',
                'new': 'Crear un nuevo Atributo'
            },
            'search': {
                'fields': ['name'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                dfield('name', 'Nombre del Atributo')
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_patronatributo',
        name='form estandar para inventario.patronatributo',
        model_ref='inventario.patronatributo',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/patronatributo/',
                'form_path': 'inventario.patronatributo'
            },
            'header': [
            ],
            'fields': [
                dfield('name', 'Nombre del Atributo'),
                o2m('valor_ids', fields=[
                    dfield('name', 'Valor (Opciones)'),
                ])
            ]
        }
    ),


    # patron valor
    dict(
        ref='list_view_standard_inventario_patronvalor',
        name='lista estandar para inventario.patronvalor',
        model_ref='inventario.patronvalor',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/patronvalor/',
                'new': 'Crear un nuevo Valor'
            },
            'search': {
                'fields': ['name', 'atributo_id'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                dfield('name', 'Valor'),
                m2o('atributo_id', 'Atributo',
                    api_route='/inventario/patronatributo/')
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_patronvalor',
        name='form estandar para inventario.patronvalor',
        model_ref='inventario.patronvalor',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/patronvalor/',
                'form_path': 'inventario.patronvalor'
            },
            'header': [
            ],
            'fields': [
                dfield('name', 'Valor'),
                m2o('atributo_id', 'Atributo',
                    api_route='/inventario/patronatributo/')
            ]
        }
    ),

    # patron
    dict(
        ref='list_view_standard_inventario_patron',
        name='lista estandar para inventario.patron',
        model_ref='inventario.patron',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/patron/',
                'new': 'Crear un nuevo Patrón'
            },
            'search': {
                'fields': ['name'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                dfield('name', 'Nombre del Patrón')
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_patron',
        name='form estandar para inventario.patron',
        model_ref='inventario.patron',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/patron/',
                'form_path': 'inventario.patron'
            },
            'header': [
            ],
            'fields': [
                dfield('name', 'Nombre del Patrón'),
                o2m('patronlinea_ids', fields=[
                    m2o('atributo_id', 'Atributo',
                        api_route='/inventario/patronatributo/'),
                    m2m('valor_ids', 'Valores',
                        api_route='/inventario/patronvalor/'),
                ])
            ]
        }
    ),

    # patron linea
    dict(
        ref='list_view_standard_inventario_patronlinea',
        name='lista estandar para inventario.patronlinea',
        model_ref='inventario.patronlinea',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/inventario/patronlinea/',
                'new': 'Crear una Línea nueva'
            },
            'search': {
                'fields': ['str_name', 'patron_id', 'atributo_id'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                dfield('str_name', 'Nombre'),
                m2o('patron_id', 'Patrón',
                    api_route='/inventario/patron/'),
                m2o('atributo_id', 'Atributo',
                    api_route='/inventario/patronatributo/'),
            ]
        }
    ),
    dict(
        ref='form_view_standard_invetario_patronlinea',
        name='form estandar para inventario.patronlinea',
        model_ref='inventario.patronlinea',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/inventario/patronlinea/',
                'form_path': 'inventario.patronlinea'
            },
            'header': [
            ],
            'fields': [
                m2o('patron_id', 'Patrón',
                    api_route='/inventario/patron/'),
                m2o('atributo_id', 'Atributo',
                    api_route='/inventario/patronatributo/'),
                m2m('valor_ids', api_route='/inventario/patronvalor/', fields=[
                    dfield('name', 'Valores')
                ])
            ]
        }
    ),



]
