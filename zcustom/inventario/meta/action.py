ACTION = [
    # PRODUCTS
    dict(
        ref='action_standard_inventario_products',
        name='vista estandar SINGLE para inventario.product',
        view_mode='inventario.product[]',
        view_refs=[
            'list_view_standard_inventario_product',
            'form_view_standard_invetario_product'
        ]
    ),

    # PRODUCTS ATRIBUTE LINES
    dict(
        ref='action_standard_inventario_productattributeline',
        name='vista estandar SINGLE para inventario.productattributeline',
        view_mode='inventario.productattributeline[]',
        view_refs=[
            'list_view_standard_inventario_productattributeline',
            'form_view_standard_invetario_productattributeline'
        ]
    ),


    # PATRONES
    dict(
        ref='action_standard_inventario_patronatributo',
        name='vista estandar SINGLE para inventario.patronatributo',
        view_mode='inventario.patronatributo[]',
        view_refs=[
            'list_view_standard_inventario_patronatributo',
            'form_view_standard_invetario_patronatributo'
        ]
    ),
    dict(
        ref='action_standard_inventario_patronvalor',
        name='vista estandar SINGLE para inventario.patronvalor',
        view_mode='inventario.patronvalor[]',
        view_refs=[
            'list_view_standard_inventario_patronvalor',
            'form_view_standard_invetario_patronvalor'
        ]
    ),
     dict(
        ref='action_standard_inventario_patron',
        name='vista estandar SINGLE para inventario.patron',
        view_mode='inventario.patron[]',
        view_refs=[
            'list_view_standard_inventario_patron',
            'form_view_standard_invetario_patron'
        ]
    ),
     dict(
        ref='action_standard_inventario_patronlinea',
        name='vista estandar SINGLE para inventario.patronlinea',
        view_mode='inventario.patronlinea[]',
        view_refs=[
            'list_view_standard_inventario_patronlinea',
            'form_view_standard_invetario_patronlinea'
        ]
    ),

]
