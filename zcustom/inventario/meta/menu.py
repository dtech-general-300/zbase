MENU = [

    # DESK
    dict(
        ref='desk_modules',
        name='modulos',
        menu_type='desk',
        sequence=100,
    ),

    # MODULE
    dict(
        ref='module_inventario',
        name='Inventario',
        menu_type='module',
        sequence=100,
        icon='list_alt',
        client_route='inventario',
        parent_ref='desk_modules',
        initial_ref='inventario_product_standard'
    ),

    # LABELS
    dict(
        ref='inventario_principal_menu',
        name='Datos Principales',
        menu_type='label',
        sequence=100,
        icon='dynamic_feed',
        parent_ref='module_inventario'
    ),
    dict(
        ref='inventario_config_menu',
        name='Configuración',
        menu_type='label',
        sequence=200,
        icon='settings',
        parent_ref='module_inventario'
    ),

    # IN-MODULE MENUS

    # productos

    dict(
        ref='inventario_product_standard',
        name='Productos',
        menu_type='model',
        sequence=90,
        icon='layers',
        client_route='productos',
        action_ref='action_standard_inventario_products',
        parent_ref='module_inventario'
    ),

    dict(
        ref='inventario_attribule_lines_standard',
        name='Atributos',
        menu_type='model',
        sequence=200,
        client_route='atributos',
        action_ref='action_standard_inventario_productattributeline',
        parent_ref='inventario_principal_menu'
    ),

    # patrones

    dict(
        ref='inventario_patron_standard',
        name='Patrones',
        menu_type='model',
        sequence=100,
        client_route='patron',
        action_ref='action_standard_inventario_patron',
        parent_ref='inventario_config_menu'
    ),

    dict(
        ref='inventario_patronlinea_standard',
        name='Líneas de Patrón',
        menu_type='model',
        sequence=200,
        client_route='patron_linea',
        action_ref='action_standard_inventario_patronlinea',
        parent_ref='inventario_config_menu'
    ),

    dict(
        ref='inventario_patronatributo_standard',
        name='Atributos de Patrón',
        menu_type='model',
        sequence=300,
        client_route='patron_atributo',
        action_ref='action_standard_inventario_patronatributo',
        parent_ref='inventario_config_menu'
    ),

    dict(
        ref='inventario_patronvalor_standard',
        name='Valores de Atributo',
        menu_type='model',
        sequence=400,
        client_route='patron_valor',
        action_ref='action_standard_inventario_patronvalor',
        parent_ref='inventario_config_menu'
    ),



]
