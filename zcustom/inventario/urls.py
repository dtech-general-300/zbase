from django.urls import include, path
from rest_framework import routers
from . import views


ROUTER = routers.SimpleRouter()

# productos
ROUTER.register(r'products', views.ProductViewSet, basename="products")
ROUTER.register(r'atributos', views.ProductAttributeLineViewSet, basename="atributos")

# patrones
ROUTER.register(r'patronatributo', views.PatronAtributoViewSet, basename="patronatributo")
ROUTER.register(r'patronvalor', views.PatronValorViewSet, basename="patronvalor")
ROUTER.register(r'patron', views.PatronViewSet, basename="patron")
ROUTER.register(r'patronlinea', views.PatronLineaViewSet, basename="patronlinea")


# Namespace
# don't use namespaces with HyperlinkedSerializers
# HyperlinkedSerializer: is the url with the id
app_name = 'inventario'

# Urls
urlpatterns = [
    path('', include(ROUTER.urls)),
]
