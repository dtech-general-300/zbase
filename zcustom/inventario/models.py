from django.db import models
from django.db.models import CharField, Value as V
from comun.models import Dbase

# PRDOUCTOS


class Product(Dbase):
    """ Clase base para cualquier tipo de producto o servicio """

    name = models.CharField('Nombre Producto', max_length=200)


class ProductAttributeLine(Dbase):
    """ Lineas de attributo para productos """

    name = models.CharField('Nombre Línea', max_length=100, null=True)
    product_id = models.ForeignKey(
        Product,
        null=True,
        on_delete=models.CASCADE,
        related_name='attribute_line_ids')


# PATRONES

class Patron(Dbase):
    """ Es el patron de un tipo de producto """

    name = models.CharField('Nombre del Patrón', max_length=100)


class PatronAtributo(Dbase):
    """ Es el atributo que puede tener una linea de patron """

    name = models.CharField('Nombre', max_length=100)


class PatronValor(Dbase):
    """ Es el valor de un atributo """

    name = models.CharField('Nombre', max_length=100)
    atributo_id = models.ForeignKey(
        PatronAtributo,
        on_delete=models.CASCADE,
        related_name='valor_ids')


class PatronLinea(Dbase):
    """ Líneas de cada patrón """

    patron_id = models.ForeignKey(
        Patron,
        on_delete=models.CASCADE,
        related_name='patronlinea_ids')
    atributo_id = models.ForeignKey(
        PatronAtributo,
        null=True,
        on_delete=models.SET_NULL)
    valor_ids = models.ManyToManyField(
        PatronValor,
        related_name='line_ids')

    @staticmethod
    def str_meta():
        return ['_:patron_id__name', 'v: - ', '_:atributo_id__name']
