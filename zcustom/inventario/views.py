from comun.views import DViewSet
from .models import (
    Product, ProductAttributeLine,
    PatronValor, PatronAtributo, Patron, PatronLinea
)
from .serializers import (
    ProductSerializer, ProductAttributeLineSerializer,
    PatronValorSerializer, PatronAtributoSerializer, PatronSerializer, PatronLineaSerializer
)

# PRODUCTOS


class ProductViewSet(DViewSet):
    """
    API endpoint: allows records to be viewed or edited.
    """
    query_model = Product
    serializer_class = ProductSerializer


class ProductAttributeLineViewSet(DViewSet):
    """
    API endpoint: allows records to be viewed or edited.
    """
    query_model = ProductAttributeLine
    serializer_class = ProductAttributeLineSerializer


# PATRONES

class PatronAtributoViewSet(DViewSet):
    """ viewset for patron atributo """
    query_model = PatronAtributo
    serializer_class = PatronAtributoSerializer


class PatronValorViewSet(DViewSet):
    """ viewset for patron valor """
    query_model = PatronValor
    serializer_class = PatronValorSerializer


class PatronViewSet(DViewSet):
    """ viewset for patron """
    query_model = Patron
    serializer_class = PatronSerializer


class PatronLineaViewSet(DViewSet):
    """ viewset for patron valor """
    query_model = PatronLinea
    serializer_class = PatronLineaSerializer
