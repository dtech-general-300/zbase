from django.apps import AppConfig


class InventarioConfig(AppConfig):
    name = 'zcustom.inventario'
