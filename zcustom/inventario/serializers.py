from comun.serializers import Dserializer, Nserializer, m2o_field
from .models import Product, ProductAttributeLine, Patron, PatronAtributo, PatronValor, PatronLinea


# PRODUCT

class NestedAttributeLineSerializer(Dserializer):
    """ Serializer to have attr lines """

    class Meta(Dserializer.Meta):
        model = ProductAttributeLine
        fields = Dserializer.Meta.fields + [
            'name'
        ]


class ProductSerializer(Nserializer):
    """ Serializer to have Products """

    attribute_line_ids = NestedAttributeLineSerializer(
        many=True, required=False)

    class Meta(Nserializer.Meta):
        model = Product
        fields = Nserializer.Meta.fields + [
            'name',
            'attribute_line_ids'
        ]


# PRODUCT ATTRIBUTE LINES

class ProductAttributeLineSerializer(Nserializer):
    """ Serializer to have attributes """

    product_id = m2o_field(Product, allow_null=True, required=False)

    class Meta(Nserializer.Meta):
        model = ProductAttributeLine
        fields = Dserializer.Meta.fields + [
            'name',
            'product_id'
        ]


###
# PATRONES
###

# patron atributo
class NestedPatronValorSerializer(Dserializer):
    """ serializer for nested patron valor """

    class Meta(Dserializer.Meta):
        model = PatronValor
        fields = Dserializer.Meta.fields + [
            'name'
        ]


class PatronAtributoSerializer(Nserializer):
    """ Serializer for patron atributo """

    valor_ids = NestedPatronValorSerializer(
        many=True, required=False)

    class Meta(Nserializer.Meta):
        model = PatronAtributo
        fields = Nserializer.Meta.fields + [
            'name',
            'valor_ids'
        ]


# patron valor
class PatronValorSerializer(Nserializer):
    """ Serializer para patron valor """

    atributo_id = m2o_field(PatronAtributo, allow_null=True, required=False)

    class Meta(Nserializer.Meta):
        model = PatronValor
        fields = Dserializer.Meta.fields + [
            'name',
            'atributo_id'
        ]


# patron linea
class PatronLineaSerializer(Nserializer):
    """ Serializer para patron linea """

    patron_id = m2o_field(Patron, read_only=False, allow_null=True, required=False)
    atributo_id = m2o_field(PatronAtributo, allow_null=True, required=False)
    valor_ids = NestedPatronValorSerializer(many=True)

    class Meta(Nserializer.Meta):
        model = PatronLinea
        fields = Dserializer.Meta.fields + [
            'patron_id',
            'atributo_id',
            'valor_ids'
        ]


# patron
class NestedPatronLineaSerializer(Dserializer):
    """ serializer for nested PatronLinea """

    atributo_id = m2o_field(PatronAtributo, allow_null=True, required=False)
    valor_ids = NestedPatronValorSerializer(many=True)

    class Meta(Dserializer.Meta):
        model = PatronLinea
        fields = Dserializer.Meta.fields + [
            'atributo_id',
            'valor_ids'
        ]


class PatronSerializer(Nserializer):
    """ Serializer for patron """

    patronlinea_ids = NestedPatronLineaSerializer(
        many=True, required=False)

    class Meta(Nserializer.Meta):
        model = Patron
        fields = Nserializer.Meta.fields + [
            'name',
            'patronlinea_ids'
        ]
