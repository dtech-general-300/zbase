from django.apps import AppConfig
from scheduler import config


class ApiAuthConfig(AppConfig):
    name = 'api_auth'

    def ready(self):
        # just import signals when use decorator @receiver,
        # each receiver prevent run twice with dispatch_uid
        from .signals import (
            user_logged_in_handler)

        # JOBS: remember to put scheduler app, first at INSTALLED_APPS because
        # instance_all_jobs needs job_setup that resides in scheduler app, and
        # because run_once_process var is needed, to instance jobs only once.
        if config.RUN_ONCE_PROCESS:
            from . import jobs
            jobs.instance_jobs()
