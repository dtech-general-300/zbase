from django.db import models
from django.conf import settings
from django.contrib.sessions.models import Session


# Authentication

class UserSession(models.Model):
    """
    model created for mapping sessions with users in order to have
    a better way to know which session belong to which user.
    on_delete=models.CASCADE
    """
    user_id = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    session_id = models.ForeignKey(Session, models.CASCADE)


# Methods

def delete_user_sessions(user):
    """
    method to delete mapped sessions,
    get mapped session of an specific user
    """
    user_sessions = UserSession.objects.filter(user_id=user)
    for user_session in user_sessions:  # iterate mapped session
        user_session.session_id.delete()  # delete session and mapped session (cascade)
