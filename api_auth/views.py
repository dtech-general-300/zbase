from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.contrib.sessions.models import Session

from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from comun.views import DViewSet
from .serializers import UserSerializer, GroupSerializer


# Authentication views

def response_data(request, from_db=False):
    """
    Return user data + expire-date, this method only work when user is authenticated,
    because request.user could be a SimpleLazyObject that only return user data correctly
    when user have been authenticated.
    Regarding  to expire-date, if from_db true, get the expire date from database
    (in verify endpoint) and not from get_expiry_date which only compute the expire-date
    (which is suitable for login endpoint).
    """
    if from_db:
        exp = Session.objects.get(pk=request.session.session_key).expire_date
    else:
        exp = request.session.get_expiry_date()

    return Response({
        'message': 'valid',
        'user': {
            'id': request.user.id,
            'username': request.user.username,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'expires': exp
        }
    }, status=status.HTTP_200_OK)


class VerifyView(views.APIView):
    """
    API endpoint: verify is the request has a valid session id received with cookie,
    then return the user data and session expire-date only if the session_id was ok,
    or an error if the session doesn't exist.
    TODO for security, this endpoint must ensure for the request ip or some
    identification, a waiting time to refresh in seconds to avoid brute force attacks.
    Maybe this is not necesary, because it needs the request have the session_id from
    cookie, but i don't know yet if anyone could make a request with a cookie with the
    same pattern of our system ( i think it is possible, and we have to think in the
    worst scene or situation always).
    """

    # pylint: disable=unused-argument
    def get(self, request, format=None):
        """ get method to verify session, this code runs only when user have been authenticated """
        return response_data(request, True)


class LoginView(views.APIView):
    """
    API endpoint: allows users to login and have a session,
    TODO restringir number of logins, and time waiting between attemps,
    TODO social authentication
    """

    permission_classes = [AllowAny]

    # pylint: disable=unused-argument
    def post(self, request, format=None):
        """ post method used to login, not use get method to login """
        try:
            username = request.data.get('username', '')
            password = request.data.get('password', '')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                # TODO customize in UI the session expiry time
                request.session.set_expiry(360000)  # seconds
                login(request, user)
                return response_data(request)

            # 403 will cause angular http observables throw error
            return Response({
                'message': 'invalid',
            }, status=status.HTTP_403_FORBIDDEN)

        # this will catch any exception
        except:
            # 500 will cause angular http observables throw error
            return Response({
                'message': 'internal',
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LogoutView(views.APIView):
    """
    API endpoint: allows loged-in users to logout
    """

    # pylint: disable=unused-argument
    def post(self, request, format=None):
        """ post method user to logout, not use get method to logout """
        try:
            logout(request)  # using Django logout
        except:  # this will catch any exception
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(status=status.HTTP_200_OK)


# General Views

class UserViewSet(DViewSet):
    """
    API endpoint: allows users to be viewed or edited.
    """
    query_model = User
    serializer_class = UserSerializer


class GroupViewSet(DViewSet):
    """
    API endpoint: allows groups to be viewed or edited.
    """
    query_model = Group
    serializer_class = GroupSerializer
