from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in
from .models import UserSession


# SIGNALS
# In a nutshell, signals allow certain senders to notify a set of
# receivers that some action has taken place. They’re especially
# useful when many pieces of code may be interested in the same events.
# https://docs.djangoproject.com/en/2.2/topics/signals/
# https://docs.djangoproject.com/en/2.2/topics/signals/#connecting-receiver-functions



# pylint: disable=unused-argument
@receiver(user_logged_in, dispatch_uid="[api_auth]user_logged_in_handler")
def user_logged_in_handler(sender, request, user, **kwargs):
    """ Authentication signal handler: Received when a user logs-in successfully.
        When user has logged in succesfully, map the user with the session.

        signal used: user_logged_in
        dispatch_uid: avoid this receiver function, to be registered twice in ready method
    """
    UserSession.objects.get_or_create(
        user_id=user,
        session_id_id=request.session.session_key
    )
