from django.urls import include, path
from rest_framework import routers
from . import views


ROUTER = routers.SimpleRouter()
ROUTER.register(r'users', views.UserViewSet, basename="users")
ROUTER.register(r'groups', views.GroupViewSet, basename="groups")

# Namespace
# don't use namespaces with HyperlinkedSerializers
# HyperlinkedSerializer: is the url with the id
app_name = 'api_auth'

# Urls
urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('verify/', views.VerifyView.as_view(), name='verify'),
    path('', include(ROUTER.urls)),
]
