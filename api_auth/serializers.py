from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """ Serializer to have a list"""

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_active',
            'last_login',
            'groups'
        ]


class GroupSerializer(serializers.ModelSerializer):
    """ Serializer to have a """
    class Meta:
        model = Group
        fields = [
            'id',
            'name'
        ]
