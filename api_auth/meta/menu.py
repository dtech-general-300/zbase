MENU = [

    # DESK
    dict(
        ref='desk_settings',
        name='configuración',
        menu_type='desk',
        sequence=1000,
    ),

    # MODULE
    dict(
        ref='module_settings',
        name='Configuración',
        menu_type='module',
        sequence=100,
        icon='settings_applications',
        client_route='configuracion',
        parent_ref='desk_settings',
        initial_ref='settings_group_default'
    ),

    # IN-MODULE MENUS
    dict(
        ref='settings_user_default',
        name='Usuarios',
        menu_type='model',
        sequence=100,
        icon='account_circle',
        client_route='usuarios',
        action_ref='action_standard_user',
        parent_ref='module_settings'
    ),
    dict(
        ref='settings_group_default',
        name='Grupos',
        menu_type='model',
        sequence=200,
        icon='group',
        client_route='grupos',
        action_ref='action_standard_group',
        parent_ref='module_settings'
    ),

]
