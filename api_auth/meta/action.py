ACTION = [
    # GROUPS
    dict(
        ref='action_standard_group',
        name='vista estandar SINGLE para groups',
        view_mode='auth.group[]',
        view_refs=[
            'list_view_standard_group',
            'form_view_standard_group'
        ]
    ),

    # USERS
    dict(
        ref='action_standard_user',
        name='vista estandar SINGLE para users',
        view_mode='auth.user[]',
        view_refs=[
            'list_view_standard_user',
            'form_view_standard_user'
        ]
    )
]
