from metadata.models import dfield

VIEW = [
    # GROUPS
    dict(
        ref='list_view_standard_group',
        name='lista estandar para groups',
        model_ref='auth.group',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/api_auth/groups/',
                'new': 'Crear un nuevo Grupo'
            },
            'search': {
                'fields': ['id', 'name'],
                'filters':[],
                'groups':[]
            },
            'fields': [
                # dfield('id', 'idd'),
                dfield('name', 'Nombre del Grupo'),
            ]
        }
    ),
    dict(
        ref='form_view_standard_group',
        name='form estandar para groups',
        model_ref='auth.group',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/api_auth/groups/',
                'form_path': 'group'
            },
            'header': [
            ],
            'fields': [
                dfield('id', 'idd'),
                dfield('name', 'Nombre del Grupo'),
            ]
        }
    ),

    # USERS
    dict(
        ref='list_view_standard_user',
        name='lista estandar para users',
        model_ref='auth.user',
        view_type='list',
        arch={
            'arguments': {
                'api_route': '/api_auth/users/',
                'new': 'Crear un nuevo Usuario'
            },
            'search': [
            ],
            'fields': [
                dfield('username', 'Usuario'),
                dfield('first_name', 'Nombre'),
                dfield('last_name', 'Apellido'),
                dfield('email', 'Correo electrónico'),
                dfield('last_login', 'Último acceso'),
            ]
        }
    ),
    dict(
        ref='form_view_standard_user',
        name='form estandar para users',
        model_ref='auth.user',
        view_type='form',
        arch={
            'arguments': {
                'api_route': '/api_auth/users/',
                'form_path': 'user'
            },
            'header': [
            ],
            'fields': [
                dfield('first_name', 'Nombre'),
                dfield('last_name', 'Apellido'),
                dfield('username', 'Usuario'),
                dfield('email', 'Correo electrónico'),
                dfield('is_active', 'Activo?'),
                dfield('last_login', 'Último acceso'),
            ]
        }
    ),
]
