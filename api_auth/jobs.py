import logging
from importlib import import_module
from django.conf import settings
from scheduler.models import job_setup, scheduler_job

LOGI = logging.getLogger("info")  # debug info warning
LOGX = logging.getLogger("error")  # error critical


# Authentication Methods:

def instance_jobs():
    """
    synchro database with this jobs, and start those that
    have already been running or have state = running.
    Use job_setup for each method to schedule.
    """
    job_setup(
        'api_auth.jobs:clear_expired_sessions',
        'Remover todas las sesiones de usuario caducas',
        'interval',
        {'hours': 24}
    )


# Authentication Jobs:

@scheduler_job
def clear_expired_sessions():
    """
    this code is copied from django source code:
    django/contrib/sessions/management/commands/clearsessions.py
    it allows to clear all expired sessions
    """
    engine = import_module(settings.SESSION_ENGINE)
    try:
        # at least this work with default db session engine
        engine.SessionStore.clear_expired()
    except NotImplementedError:
        LOGX.error("Session engine %s doesn't support clearing expired sessions.\n",
                   settings.SESSION_ENGINE)
