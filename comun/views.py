from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from .filters import get_filtered_query, get_ordered_query
from .pagination import DLimitPagination


# CUSTOM MODEL VIEW SET

# Viewset:  list(get) create(post) retrieve(get) update(put) partial_update(patch) delete(destroy)
# actions: https://www.django-rest-framework.org/api-guide/routers/#routing-for-extra-actions
# actions: https://www.django-rest-framework.org/api-guide/viewsets/#marking-extra-actions-for-routing
# *si el action es detail=true debe ir pk=None porque en la url el router la agregara /api/<pk>/method_name
# y es pk=None por convencion. Cuando detail=False no se pone pk=None porque no está en la url.
# *si se desea cambiar el url_path (que por default es method_name) del endpoint, de debe agregar al
# decorador esto <url_path='new_name'> esto termina en /api/<pk>/new_name o /api/new_name
# *si se desea cambiar el name de la url se usa url_name='new_name', termina en {basename}-{new_name}
# *si el action no tiene 'method=..' el default es GET


class DViewSet(viewsets.ModelViewSet):
    """
    ViewSet that provide custom actions.
    """

    # PAGINATION

    pagination_class = DLimitPagination

    # FILTERS

    query_model = None

    def get_queryset(self):
        """
        Override get_quetyset in order to apply custom filters
        Use query_model para indicar el modelo (models.Model)
        """
        res = get_filtered_query(self)
        res = get_ordered_query(self, res)
        return res

    # DESTROY MANY

    @action(detail=False, methods=['delete'])
    def destroy_many(self, request):
        """ delete the filtered queryset TODO: error handler """
        queryset = self.get_queryset()
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
