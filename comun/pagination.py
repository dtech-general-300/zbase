from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination
from rest_framework.response import Response


class DNumberPagination(PageNumberPagination):
    """ Custom paginator with page number reference """

    page_size = 80  # override this in order to change default list page size in each model
    max_page_size = 1000
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'previous': self.prev_n(),
            'current': self.page.number,
            'next': self.next_n(),
            'results': data,
            'size': self.page.paginator.per_page,
            'count': self.page.paginator.count,
        })

    def next_n(self):
        """ next page number """
        if not self.page.has_next():
            return None
        return self.page.next_page_number()

    def prev_n(self):
        """ previous page number """
        if not self.page.has_previous():
            return None
        return self.page.previous_page_number()


class DLimitPagination(LimitOffsetPagination):
    """ Custom paginator with offset reference """

    default_limit = 80  # override this in order to change default list page size in each model
    max_limit = 1000

    def get_paginated_response(self, data):
        return Response({
            'previous': self.prev_n(),
            'current': self.offset,
            'next': self.next_n(),
            'results': data,
            'limit': self.limit,
            'count': self.count,
        })

    def next_n(self):
        """ next offset """
        # return None le angular knows that paginator is at right limit, so angular put 0
        if self.offset + self.limit >= self.count:
            return None
        return self.offset + self.limit

    def prev_n(self):
        """ previous offset """
        # return None let angular knows that paginator is at left limit, so angular computes last value
        if self.offset <= 0:
            return None
        # if offset is not at left limit, return a new offset trimmed to left limit (0) if negative
        if self.offset - self.limit <= 0:
            return 0

        return self.offset - self.limit
