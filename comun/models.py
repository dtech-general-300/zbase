from django.contrib.auth.models import User
from django.db import models
from django.db.models import F, CharField, Value as V
from django.db.models.functions import Concat


class Dbase(models.Model):
    """
    this file includes the base behaviour for all classes. It uses abstract, which
    make possible to override the default models.Model class of django, in order to
    add field or methods. If you want to override the methods use super, if you want
    to override the fields redefine them, it you don want the fields put None.
    """
    # NAME
    # all models must have a name field ('cause everything has a name). I you want to not have name, override str_meta
    # IMPORTANT: __str__ cannot use str_annotate to retrieve the string because it will provoke that when frontend
    # query a listview, django hits the database once per record (that's very very bad to performance). Also, str_annotate
    # cannot use __str__ because django not allow that, it only allow to use annotation to compute the str_name, there is one
    # one posibility to iterate objects.all() and apply to each iteration the function str(object), but that seems to be very
    # bad to performance too, the database is faster when we use annotate, so the best is to keep separated.
    # One solution to have one source of thruth: str_meta
    name = None

    def __str__(self):
        """ IMPORTANT: must be equivalen to str_annotate. if the model not has name: override __str__ and str_meta """
        def get_nested(lookup):  # example: get self.some_id.name from some_id__name
            splits = lookup.split('__')
            data = self
            for split in splits:
                data = getattr(data, split)
            return data

        def process(meta):  # process the metadata
            splits = meta.split(':')
            if splits[0] in ['_', 'f']:
                return get_nested(splits[1])
            elif splits[0] == 'v':
                return splits[1]

        final_str = ''
        for meta in self.__class__.str_meta():
            final_str += process(meta)

        return final_str

    @staticmethod
    def str_meta():
        """
        IMPORTANT: must be equivalent to __str__, str_meta just return metadata for operations
        Format: all is split by ':', posicion [0] indica la funcion (_ es el string puro), el resto
        los args en orden, todas las funciones estan en process dentro de str_annotate
        """
        return ['f:name']

    @classmethod
    def str_annotate(cls, related_prefix=''):
        """
        IMPORTANT: must be equivalent to __str__. 'related_prefix' allow to call This F function with a
        prefix of related path for filter purposes. Never override this method, add function if you want.
        """
        def process(meta):
            splits = meta.split(':')
            if splits[0] == '_':
                return related_prefix + splits[1]
            elif splits[0] == 'f':
                return F(related_prefix + splits[1])
            elif splits[0] == 'v':
                return V(*splits[1:])

        meta_array = cls.str_meta()  # cls get the class (the class which inherit Dbase)

        if len(meta_array) == 1:
            # because concat only accept at least 2 values
            return process(meta_array[0])
        else:
            for index, meta in enumerate(meta_array):
                meta_array[index] = process(meta)
            return Concat(*meta_array, output_field=CharField())

    # OTHER FIELDS

    is_active = models.BooleanField(default=True, db_index=True)
    create_date = models.DateTimeField('Creación', auto_now_add=True)
    write_date = models.DateTimeField('Modificación', auto_now=True)
    create_uid = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='created_%(app_label)s_%(class)s')
    write_uid = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='modified_%(app_label)s_%(class)s')

    class Meta:
        abstract = True
