import re
from ast import literal_eval
from django.db.models import Q

########################
# FILTERS
########################


# examples:
# ?query=[('holi','=',5.3),'|',('holi','=',5.4),'^', [('yes', 'in', 'ohno'),'|', '!',('t','=',9)]]
# ?query=[('model__app_label','exact','auth'),'^',('model__model','exact','user'),'|',('some','in',[1,2])]

def get_filtered_query(view):
    """
    return a filtered queryset, it uses the format: ?query=[...] the ... indicates elements
    of array that could be operators: not='!' or='|' and='^'; groups with [], they could group
    operators and tuples; and the third element type is a tuple that indicates django lookups
    like ('lhs','lookup_name', 'rhs')  (django: <lhs>__<lookup_name>=<rhs>), lhs could be any
    field and could use __ to related fields, the lookup name could be any one that django offers
    and the rhs ...(only plain, but in the future will be F())
    TODO rhs with F, count sum etc...
    TODO acumulate filters, exlucdes, aggregate(groupby), orderby, etc.
    """
    model = view.query_model
    objects = model.objects
    query = view.request.query_params.get('query', None)

    # avoid filter when no query
    if query is None:
        return objects.all()

    # add an annotate when query want str (this happen in m2o request or may happen in other cases)
    if 'str_name' in query:
        objects = annotate_str(model, query)

    qlist = []
    query = literal_eval(query)
    response = evaluate(query, qlist)

    # pylint: disable=eval-used
    # notes: eval is safe because the input 'response' es obtainded by my own code in evaluate,
    # appart from that, response uses the qlist var, which is only formed by a Q() function which
    # not allow 'malicious' code, appart from that, the Q() functions are built with expression
    # values from tuples and that values como from literal_eval which is a safely method. So, the
    # eval function only uses response (safe) and qlist (safe), but we also include in eval, the
    # globals (second argument) like {'__builtins__': None} in order to avoid many malicious code
    # and also locals (third argument) with {'qlist': qlist}, which only allow to use qlist var.
    # another security in other case will be response.replace('.__base__','').replace('lambda','').
    # .replace('.__builtins__','').replace('__subclasses__',''), but that is for other cases.
    qdomain = eval(response, {'__builtins__': None}, {'qlist': qlist})
    return objects.filter(qdomain)


def annotate_str(model, query):
    """ add computed column 'str_name' in the query set (or related queryset) to be filtered, base on model method """

    # return list of matches lhs (lefthandside) of tuple between quotes, with str_name (with all it has before it)
    # example [('attr_id__str_name__icontains')] return: attr_id__str_name in order to annotate easily
    # pylint: disable=anomalous-backslash-in-string
    res = re.findall("\('(\w*str_name)\w*?'", query)

    to_annotate = {}
    for value in res:  # itera los resultados
        splits = value.split('__')  # divide en substrings segun __

        # añade al dict si es necesario y selecciona las clases relacionadas hasta llegar a str_name
        # si str_name esta al inicio utiliza directamente model sin buscar ninguna clase relacionada
        # funciona para foreigKeys, y related_prefix ayuda a que funcionen las funcinoes F en str_annotate
        last_model = model
        related_prefix = ''
        for sub in splits:
            if sub != 'str_name':
                last_model = last_model._meta.get_field(sub).related_model
                related_prefix += sub + '__'
        if value not in to_annotate:
            to_annotate[value] = last_model.str_annotate(related_prefix)
    print(to_annotate)
    return model.objects.annotate(**to_annotate)


def evaluate(expression, reflist):
    """
    recursive method that allows to build the same initial expression, to a string with
    qlist[i], groups with (), and operators. It will make possible to eval with the qlist var
    """
    response = ''
    if type(expression).__name__ == 'list':
        response = '('
        for item in expression:
            response += evaluate(item, reflist)
        response += ')'
    elif type(expression).__name__ == 'str':
        response += expression.replace('^', '&').replace('!', '~')
    elif type(expression).__name__ == 'tuple':
        # <lhs>__<lookup_name>=<rhs>
        reflist.append(Q(**{expression[0]+'__'+expression[1]: expression[2]}))
        response += 'qlist[' + str(len(reflist)-1) + ']'
    return response


########################
# ORDER
########################

# example:
# ?order=['field1','-field2','field3__related']

def get_ordered_query(view, query):
    """
    Order a queryset with the fields specified in queryparams
    """
    order = view.request.query_params.get('order', None)
    if order is None:
        return query
    order = literal_eval(order)  # literal_eval is a safe method
    return query.order_by(*order)
