from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
# from django.contrib.auth.models import User


class CommonMixin:
    """ common behaviour for serializers """

    str_name = serializers.StringRelatedField(source='__str__')

    # META

    class Meta:
        """ MetaData for serializer """
        model = None
        fields = ['id', 'str_name']

    # CREATE & UPDATE

    def create(self, validated_data):
        """ set crete_uid and write_uid in POST request """
        user = self.context['request'].user
        if user and user.is_authenticated:
            validated_data['create_uid'] = user
            validated_data['write_uid'] = user
        return super(CommonMixin, self).create(validated_data)

    def update(self, instance, validated_data):
        """ keep write_uid updated when PATCH is called """
        user = self.context['request'].user
        if user and user.is_authenticated:
            instance.write_uid = user
        return super(CommonMixin, self).update(instance, validated_data)


class Dserializer(CommonMixin, serializers.ModelSerializer):
    """
    Serializer to have a standard way to work with models.
    To use this serializer: class SomethingSerializer(DSerializerializer)
    To override Meta use: class Meta(DSerializerializer.Meta)
    """
    str_name = CommonMixin.str_name
    pass


class Nserializer(CommonMixin, WritableNestedModelSerializer):
    """
    Serializer to have a standard way to work with models with Nested Relational Data.
    To use this serializer: class SomethingSerializer(NSerializerializer)
    To override Meta use: class Meta(NSerializerializer.Meta)
    """
    str_name = CommonMixin.str_name
    pass


# MANY TO ONE SERIALIZER

def m2o_field(base_model, **kwargs):
    """ Allow to DRY m2o fields, it returns instance of a serializer """

    class SerializerClass(Dserializer):
        """ DRY class for many2one fields, it only has the id and __str__ """
        class Meta(Dserializer.Meta):
            model = base_model
            fields = Dserializer.Meta.fields

    return SerializerClass(**kwargs)
