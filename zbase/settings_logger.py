from django.utils.log import DEFAULT_LOGGING  # default logging config of django

# django use python built-in logging module (import logging)
# https://docs.djangoproject.com/en/2.2/topics/logging/

# To use:
# import logging
# logi = logging.getLogger("info") # debug info warning
# logx = logging.getLogger("error") # error critical
# logi.info('something happened')
# logx.critical('something really bad happened')


LOGGING = {
    'version': 1,
    # always False in order to let other loggers work
    'disable_existing_loggers': False,

    # FORMATTERS

    # are the format in which the log is record in the file, console, etc.

    'formatters': {
        'large': {
            'format': '%(asctime)s   %(levelname)s   %(process)d  %(pathname)s  %(funcName)s  %(lineno)d  %(message)s'
        },
        'tiny': {
            'format': '[%(asctime)s]   %(levelname)-8s   %(message)s'
        },
        'file': {
            'format': '[%(asctime)s]   %(levelname)s   %(name)s  %(message)s'
        },
        'server_format': DEFAULT_LOGGING['formatters']['django.server'],
    },

    # FILTERS

    # allow to conditionally apply a handler
    # here we get the filter from DEFAULT_LOGGING

    'filters': {
        'require_debug_true': DEFAULT_LOGGING['filters']['require_debug_true'],
    },

    # HANDLERS

    # Permiten manejar los mensajes registrados de los log en diferentes formas.
    # por ejemplo imprimir en la consola, guardar en un archivo rotativo.
    # Aquí también se aplican los levels.
    # Handlers use filters and formatters.

    'handlers': {
        'console': {  # for loggers: error e info, consola solo en modo debug
            'level': 'DEBUG',
            'filters': ['require_debug_true'],  # conditional handle
            'class': 'logging.StreamHandler',
            'formatter': 'tiny'
        },
        'console_server': {  # para django.server con formato original
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'server_format'
        },
        'errors_file': {  # para error en file
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 1024*1024*5,  # 5MB
            'backupCount': 1,  # numero de archivos de backup
            'filename': 'logs/Error.log',  # path para el archivo
                        'formatter': 'large',
        },
        'info_file': {  # para info en file
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 1024*1024*5,  # 5MB
                        'backupCount': 1,
                        'filename': 'logs/Info.log',
                        'formatter': 'large',
        },
        'server_file': {  # django.server y django.request en file
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 1024*1024*5,  # 5MB
            'backupCount': 1,
            'filename': 'logs/Server.log',
            'formatter': 'tiny',
        },

        # TODO it uses the email backend of django
        # 'mail_admins': {
        # 	'level': 'ERROR',
        # 	'class': 'django.utils.log.AdminEmailHandler',
        # 	'include_html': True,
        # }

        # TODO
        # 'security_file': { # csrf logger
        #     'level': 'DEBUG',
        #     'class': 'logging.handlers.RotatingFileHandler',
        # 	  'maxBytes': 1024*1024*3, # 3MB
        # 	  'backupCount': 3,
        #     'filename': 'logs/Security.log',
        #     'formatter': 'file',
        # },
    },


    # LOGGERS

    # Logger instance:
    # logi = logging.getLogger("info") # debug info warning

    # showing a message:
    # logi.info('something happened')

    # Rules:
    # *A logger is configured to have a log level.
    # when you want to show a msg: logi.info it will show because the level
    # of logi is DEBUG and level of .info is INFO, so INFO is equal or higher
    # than DEBUG and it will show or register.
    # *We have error logger, and info logger. logx.error and logx.critical
    # will be saved in one file (See handlers), and logi.debug .info .warning
    # will be saved in other file.
    # *Loggers use handlers.

    'loggers': {
        'error': {  # it allow to record developer logs, use it with: error critical
            'handlers': ['console', 'errors_file'],
            'level': 'WARNING',
            'propagate': False,
        },
        'info': {  # it allow to record developer logs, use it with: debug info warning
            'handlers': ['console', 'info_file'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django.request': {  # it allow to record the requests
            'level': 'DEBUG',
            'handlers': ['server_file'],
        },
        # TODO when this logger works?
        'django.server': {  # log request received by the server invoked by the runserver command
            'level': 'DEBUG',
            'handlers': ['console_server', 'server_file'],
            'propagate': False,
        },
        # TODO
        # 'django.security.csrf':{ # it allows to record csrf security warns
        #     'level': 'DEBUG',
        #     'handlers': ['security_file'],
        # },

    },
}
